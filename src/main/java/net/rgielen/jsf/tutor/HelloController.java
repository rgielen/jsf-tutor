package net.rgielen.jsf.tutor;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

@ManagedBean
@RequestScoped
public class HelloController implements Serializable {

	private String name;
	private int age1;
	private int age2;
	private int ageSum;
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public int getAge1() {
		return age1;
	}
	
	public void setAge1(int age1) {
		this.age1 = age1;
	}
	
	public int getAge2() {
		return age2;
	}
	
	public void setAge2(int age) {
		this.age2 = age;
	}
	
	public int getAgeSum() {
		return ageSum;
	}
	
	public String handleOkClick() {
		System.out.println(name + " - " + ageSum);
		return "";
	}
	
	public String handleSayHello() {
		return "sayHello";
	}
	
	public String handleGoodBye() {
		return "/goodbye.xhtml";
	}
	
	public String handleAddClick() {
		ageSum = age1 + age2;
		return "";
	}
	
	public String handleSubClick() {
		ageSum = age1 - age2;
		FacesContext context = getFacesContext();
		return "";
	}

	FacesContext getFacesContext() {
		return FacesContext.getCurrentInstance();
	}
	
}
