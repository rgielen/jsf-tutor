package net.rgielen.jsf.tutor;

import static org.junit.Assert.*;

import javax.faces.context.FacesContext;

import org.junit.Test;

public class HelloControllerTest {

	@Test
	public void testHandleAddClick() {
		HelloController controller = new HelloController();
		controller.setAge1(10);
		controller.setAge2(20);
		controller.handleAddClick();
		assertEquals(30, controller.getAgeSum());
	}

	@Test
	public void testHandleSubClick() {
		HelloController controller = new HelloController() {
			@Override
			FacesContext getFacesContext() {
				return null;
			}
		};
		controller.setAge1(10);
		controller.setAge2(20);
		controller.handleSubClick();
		assertEquals(-10, controller.getAgeSum());
	}

}
